# ernestbib
import os, shutil
import knuggets.utils

myPackageFile = 'ernestbib.bib'
mySourceFile = 'knugget.bib'
myScriptPath = os.path.dirname(os.path.realpath(__file__))

knuggets.utils.buildltxmod(fileexpr=mySourceFile,target=myPackageFile,path=myScriptPath)

texmf = os.path.abspath(os.path.join(myScriptPath, os.pardir, os.pardir, 'texmf', 'bibtex', 'bib', 'knuggets'))
shutil.copy(myPackageFile,texmf)
