# ernestbib
import os
import knuggets.utils

myPackageFile = 'ernestbib.bib'
mySourceFile = 'knugget.bib'
myScriptPath = os.path.dirname(os.path.realpath(__file__))

knuggets.utils.buildltxmod(fileexpr=mySourceFile,target=myPackageFile,path=myScriptPath)